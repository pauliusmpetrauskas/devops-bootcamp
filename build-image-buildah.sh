#! /bin/bash
cd frontend
buildah bud -t frontend .
buildah tag frontend:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/frontend:latest
buildah push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/frontend:latest
cd ..
cd backend/account-manager
buildah bud -t account-manager .
buildah tag account-manager:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/account-manager:latest
buildah push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/account-manager:latest
cd ..
cd branch-finder
buildah bud -t branch-finder .
buildah tag branch-finder:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/branch-finder:latest
buildah push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/branch-finder:latest
cd ..
cd plan-simulator
buildah bud -t plan-simulator .
buildah tag plan-simulator:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/plan-simulator:latest
buildah push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/plan-simulator:latest


