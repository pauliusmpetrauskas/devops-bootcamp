#! /bin/bash
cd frontend
docker build -t frontend .
docker tag frontend:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/frontend:latest
docker push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/frontend:latest
cd ..
cd backend/account-manager
docker build -t account-manager .
docker tag account-manager:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/account-manager:latest
docker push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/account-manager:latest
cd ..
cd branch-finder
docker build -t branch-finder .
docker tag branch-finder:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/branch-finder:latest
docker push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/branch-finder:latest
cd ..
cd plan-simulator
docker build -t plan-simulator .
docker tag plan-simulator:latest 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/plan-simulator:latest
docker push 039506545583.dkr.ecr.eu-west-1.amazonaws.com/bootcamp/service5/plan-simulator:latest


